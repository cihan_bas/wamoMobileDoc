# Welcome to wamo Docs 

##Why wamo use firebase notification

Used to display a visible notification on devices.

Firebase has a lot  documentation, and if you have issue you can find solution on stackoverflow and github community

Contain an optional data payload (map of key / value pairs) for consumption by the app if it is in the foreground, or if the notification is subsequently opened. If the notification is not opened, this data will never become available to the app.

Intercepted by the Mobile Device's OS.

Delivered to the notification tray when the app is in the background or closed.

##Pricing for Firebase

For notification firebase not ask any price but if we use it with cloud messaging it will have some price. You can find pricing list below link:

https://firebase.google.com/pricing/
