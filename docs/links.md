
## Markdown Syntax documents
[Markdown helper ](https://www.markdownguide.org/basic-syntax).

## Storybook Convert error

[React native fails with JSON.serialization issue](https://github.com/storybookjs/react-native/issues/13).

```javascript
//Replace
hooks.currentContext = context;
//with
hooks.currentContext = Object.assign({}, context, { hooks: null });
//in the file
node_modules/@storybook/addons/dist/hooks.js
```
## Markdown Generate Table
[Generate page ](https://www.tablesgenerator.com/markdown_tables).
