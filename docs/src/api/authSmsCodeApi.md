After phone number check, user will receive sms code and it should enter sms code to receive token to process next steps.

* **URL**

    /AuthUserSmsCode

* **Method:**

    `POST`
   
* **Data Params**

    **Required:**

      `userId = [String]`
      
      `smsCode = [Integer]`

* **Success Response:**

    **Code:** 200 <br />
    **Content:** 
    
```json
{
    "result": "ok",
    "timestamp": "2020-04-01 13:22:35.000000",
    "exists": true,
    "data": {
        "userId": "70b489226ec55d8b25695c7e261704e048c4728e",
        "userToken": "9ae2d62c5afe6b6d7e16ad8fabfff638f684cddc",
        "tokenExpiresIn": 120,
        "userRegistered": true,
        "emptyPasscode": false
    }
}
```

* **Error Response:**

  * **Code:** 10002 Wrong Code <br />
    **Content:** 

```json
{
    "result": "error",
    "timestamp": "2020-04-01 13:21:45.000000",
    "error": {
        "code": 10002,
        "msg": "Please try again",
        "text": "PLEASE_TRY_AGAIN"
    }
}
```