To check user's phone number you create checkPhoneNumber object to see the user is registered before or not.

* **URL**

    /PhoneNumberCheck

* **Method:**

    `POST`
   
* **Data Params**

    **Required:**
```json
{
 "mobileCode" : "[String]",
 "mobileNumber": "[String]"
}
   
```
  

* **Success Response:**

    **Code:** 200 <br />
    **Content:** 
    
```json
{
  "result": "ok",
  "timestamp": "2020-04-01 11:15:19.000000",
  "exists": true,
  "data": {
      "userId": "511962f588a638919197b2f4ce11b53be39afa82",
      "mobileCode": 90,
      "mobileNumber": 545345354,
      "mobileCountry": "TR",
      "CountryName": "Turkey",
      "userRegistered": true
   }
 }
```

* **Error Response:**

  * **Code:** 1004 FIELD REQUIRED <br />
    **Content:** 

```json
{
    "result": "error",
    "timestamp": "2020-04-01 11:19:37.000000",
    "error": {
        "code": 10004,
        "msg": "The Phone Number field is required",
        "text": "MOBILE_NUMBER_REQUIRED"
    }
}
```