If the user pass the sms code and if it not registered before, api should register it

* **URL**

    /RegisterUser

* **Method:**

    `POST`
   
* **Data Params**

    **Required:**

      `userId = [String]`
      
      `userToken = [String]`
      
      `firstName = [String]`
      
      `lastName = [String]`
      
      `email = [String]`
      
      `dateOfBirth = [date]`
      
      `type = [String]`
      
      `country = [String]`
      
      `address = [String]`
      
      `zip = [String]`
      
    **Optional:**
    
      `inviteCode = [Integer]`
      
* **Success Response:**

    **Code:** 200 <br />
    **Content:** 
    
```json
{
    "result": "ok",
    "timestamp": "2020-04-01 13:22:35.000000",
    "exists": true,
    "data": {
        "userId": "70b489226ec55d8b25695c7e261704e048c4728e",
        "userToken": "9ae2d62c5afe6b6d7e16ad8fabfff638f684cddc",
        "tokenExpiresIn": 120,
        "userRegistered": true,
        "emptyPasscode": false
    }
}
```

* **Error Response:**

  * **Code:** 10002 Already Member <br />
    **Content:** 

```json
{
    "result": "error",
    "timestamp": "2020-04-01 13:32:32.000000",
    "error": {
        "code": 10002,
        "msg": "ALREADY_MEMBERS",
        "text": "ALREADY_MEMBERS"
    }
}
```