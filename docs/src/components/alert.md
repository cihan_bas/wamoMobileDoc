A pure JavaScript React-Native dialog that follows closely the UI of its native counterpart while expanding its features

<div style='text-align:center'>
  <img src="../../../assets/gif/alert.gif" width="200" alt = 'wamo alert' >
</div>

## import Alert

```javascript
import { Alerts } from 'components';
```

## Example Alert

```javascript
<Alerts
	typeEnum={responseType.success}
	visible={true}
	subtitle='Cihan Bas send you €200'
	title='€100'
	describe='data is wrong'
	onBackdropPress={() => this.setState({ visible: false })}
	onPress={() => this.setState({ visible: false })}
/>
```

| name            | type   | default  | required | description                                                                                            |
| --------------- | ------ | -------- | -------- | ------------------------------------------------------------------------------------------------------ |
| visible         | bool   | false    | yes      | if true show modal                                                                                     |
| typeEnum        | enum   | info     | yes      | modal header icon type. export const responseType = { success: 'success', error:'error'info: 'info',}; |
| title           | string | ''       | no       | title                                                                                                  |
| subtitle        | string | ''       | no       | subtitle                                                                                               |
| describe        | string | ''       | no       | describe                                                                                               |
| buttonText      | string | 'Got it' | no       | button text                                                                                            |
| onBackdropPress | func   | void 0   | yes      | press the overlay                                                                                      |
| onPress         | func   | void 0   | no       | press the button                                                                                       |

