Bank account component displays each transaction with amount, description and title.

<p align="center">
  <img src="../../../assets/gif/bank.gif" width="200">
</p>

## Import the bankaccount 

```javascript
 
 import {BankAccount} from 'components'
 
```

## Usage with storybook

```javascript
 
import React from 'react';
import { storiesOf } from '@storybook/react-native';
import { action } from '@storybook/addon-actions';
import BankAccount from './index';
import { BufferView } from '../decorator';
import { ScrollView, View } from 'react-native';
const gbp = require('assets/img/icons/currencies/gbp.png');
const eur = require('assets/img/icons/currencies/eur.png');
const btc = require('assets/img/icons/currencies/btc.png');
const eth = require('assets/img/icons/currencies/eth.png');
const bigIcon = {
  uri:
    'https://cdn.decoist.com/wp-content/uploads/2012/05/showing-collections-in-style.jpg',
};
import { colors } from 'config';

storiesOf('BankAccount', module)
  .addDecorator(BufferView)
  .add('BankNoneValue', () => {
    return <BankAccount />;
  })
  .add('BankFullofData', () => {
    return (
      <BankAccount
        source={eur}
        title="Spotify Premium"
        subtitle="Subscriptions"
        num={2458}
      />
    );
  })
  .add('BankMultiple', () => {
    return (
      <ScrollView>
        <BankAccount source={eur} title="Euro Account" num={2458} />
        <BankAccount source={gbp} subtitle="Subscriptions" num={2458} />
        <BankAccount />
        <BankAccount
          source={btc}
          title="Spotify Premium"
          subtitle="Subscriptions"
        />
        <BankAccount
          source={eth}
          title="Spotify Premium"
          subtitle="Subscriptions"
          num={2458}
        />
        <BankAccount title="Without Icon" subtitle="Subscriptions" num={2458} />
      </ScrollView>
    );
  })
  .add('withImage', () => {
    return (
      <BankAccount
        source={bigIcon}
        title="With Image"
        subtitle="Subscriptions"
        num={2458}
      />
    );
  });
```

## Props 
| name          | type           | default | is required | description                    |
|---------------|----------------|---------|-------------|--------------------------------|
| source        | number, object | {}      | no          | source of left image           |
| bottomDivider | bool           | true    | no          | divide each row                |
| title         | string         | null    | no          | title of transaction detail    |
| subtitle      | string, number | null    | no          | subtitle of transaction detail |
| onPress       | function       | null    | no          | give action when on press row  |
| num           | string, number | 0       | no          | amount of transaction          |