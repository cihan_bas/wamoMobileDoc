Buttons are touchable elements used to interact with the screen. 
They may display text, icons, or both. 
Buttons can be styled with several props to look a specific way.

 ![wamo buttons ](../../assets/img/buttons.png)


## import the button 

```javascript
 
 import {Button} from 'components'
 
```

## Usage with storybook


```javascript
 
import React from 'react';
import { storiesOf } from '@storybook/react-native';
import { action } from '@storybook/addon-actions';
import Button from './index';
import Container from '../Container';
import { BufferView } from '../decorator';

storiesOf('Button', module)
  .addDecorator(BufferView)
  .add('defaultButton', () => {
    return <Button onPress={action('tapped-default')} title="Default" />;
  })
  .add('disabled', () => (
    <Button onPress={action('tapped-default')} title="Disable" disabled />
  ))
  .add('WithContainer', () => (
    <Container padding={true} type="active">
      <Button
        onPress={action('tapped-default')}
        title="with Icon"
        icon={{ name: 'plus', type: 'font-awesome' }}
      />
    </Container>
  ))
  .add('withIcon', () => (
    <Button
      onPress={action('tapped-default')}
      title="with Icon"
      icon={{ name: 'plus', type: 'font-awesome' }}
    />
  ))
  .add('buton-loading', () => (
    <Button onPress={action('tapped-default')} title="Loading" loading />
  ))
  .add('button-loadingwithicon', () => (
    <Button
      onPress={action('tapped-default')}
      title="Loading with icon"
      loading
      icon={{ name: 'plus', type: 'font-awesome' }}
    />
  ))
  .add('delete-button', () => (
    <Button
      onPress={action('tapped-default')}
      title="Delete"
      deleted
      type="deleted"
    />
  ))

  .add('without-text', () => (
    <Button onPress={action('tapped-default')} deleted type="deleted" />
  ))

  .add('delete-disabled', () => (
    <Button
      onPress={action('tapped-default')}
      title="delete disable"
      disabled
      deleted
      type="deleted"
    />
  ));
 
```

## Props 
| name     	| type   	| default 	| is required 	| description                                                                  	|
|----------	|--------	|---------	|-------------	|------------------------------------------------------------------------------	|
| disabled 	| bool   	| false   	| No          	| if you set true, it will be disable                             |
| title    	| string 	| ''      	| yes         	| button text                                                                  	|
| icon     	| object 	| null    	| no          	| button left icon like this icon={{ name: 'plus', type: 'font-awesome' }}     	|
| loading  	| bool   	| false   	| no          	| button loading                                                               	|
| deleted  	| bool   	| false   	| no          	| change button, default to delete button  	|
| onPress  	| func   	| null    	| yes         	| press button                                                                 	|
