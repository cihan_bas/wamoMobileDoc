Contact component displays  user contact with their phone number and name if the contact is member of wamo, wamo icon 
show of on the right side of row.

<p align="center">
  <img src="../../../assets/gif/contact.gif" width="200">
</p>


## Import the text 

```javascript
 
 import {Contact} from 'components'
 
```

## Usage with storybook

```javascript
 
 import React from 'react';
 import { storiesOf } from '@storybook/react-native';
 import Contact from './index';
 import { BufferView } from '../decorator';
 const icon = require('assets/img/icons/transactionListAvatar.png');
 const bigIcon = {
   uri:
     'https://cdn.decoist.com/wp-content/uploads/2012/05/showing-collections-in-style.jpg',
 };
 
 storiesOf('Contacts', module)
   .addDecorator(BufferView)
 
   .add('noneValue', () => {
     return <Contact />;
   })
   .add('fullofData', () => {
     return (
       <Contact
         source={icon}
         title="Daniel Tkachenko"
         subtitle="1-212-555-7575 "
         amount={2458}
         isMember
       />
     );
   })
   .add('isNotMemeber', () => {
     return (
       <Contact
         source={icon}
         title="Daniel Tkachenko"
         subtitle="1-212-555-7575 "
         isMember={false}
       />
     );
   })
   .add('withAvatar', () => {
     return (
       <Contact
         source={bigIcon}
         title="Daniel Tkachenko"
         subtitle="1-212-555-7575 "
       />
     );
   })
   .add('wihoutAvatar', () => {
     return <Contact title="Daniel Tkachenko" subtitle="1-212-555-7575 " />;
   });

```

## Props 
| name          | type           | default  | is required | description                                   |
|---------------|----------------|----------|-------------|-----------------------------------------------|
| source        | number, object | user.png | no          | source of left image                          |
| bottomDivider | bool           | true     | no          | divide each row                               |
| title         | string         | ''       | no          | title of contact detail                       |
| subtitle      | string         | ''       | no          | subtitle of contact detail                    |
| isMember      | bool           | false    | no          | shows wamo icon on right if contact is member |
