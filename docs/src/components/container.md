Container can be set for all component's container for background.

<p align="center">
  <img src="../../../assets/gif/container.gif" width="200">
</p>

## Import the bankaccount 

```javascript
 
 import {Container} from 'components'
 
```

## Usage with storybook

```javascript
 
 import React from 'react';
 import { storiesOf } from '@storybook/react-native';
 import Container from './index';
 
 storiesOf('Container', module)
   .add('default', () => {
     return <Container />;
   })
   .add('primary', () => {
     return <Container type={'primary'} />;
   })
   .add('secondary', () => {
     return <Container type={'secondary'} />;
   });
```

## Props 
| name          | type                     | default | is required | description                  |
|---------------|--------------------------|---------|-------------|------------------------------|
| padding       | bool                     | false   | no          | padding of container         |
| paddingBottom | number                   | 40      | no          | padding bottom for container |
| type          | active, primary, secondary | active  | no          | shows type of container      |