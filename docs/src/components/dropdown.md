Configurable visible item count

## ScreenShot

<img src="../../../assets/gif/dropdown.gif" alt="dropdown" width="200"/>

## Example

```javascript
import Dropdown from 'components';
```

```json
const btc = require('assets/img/icons/currencies/btc.png');
const eth = require('assets/img/icons/currencies/eth.png');
const eur = require('assets/img/icons/currencies/eur.png');
const list = [
  {
    title: 'BTC',
    img: btc,
    subtitle: 2205,
  },
  {
    title: 'ETH',
    img: eth,
    subtitle: 1500,
  },
  {
    title: 'EUR',
    img: eur,
    subtitle: 1500,
  },
  {
    title: 'EUR',
    img: eur,
    subtitle: 1500,
  },
  {
    title: 'EUR',
    img: eur,
    subtitle: 1500,
  },
  {
    title: 'EUR',
    img: eur,
    subtitle: 1500,
  },
];
```

```javascript
<DropDown
	dropdownTitle='BTC'
	onPress={action('tapped-btc')}
	dropDownImg={{ source: btc }}
	data={list}
	withAvatar
	title='Choose currency:'
	showItemCount={2}
/>
```

## Props

| name                  | type          | default  | required | description                                                        |
| --------------------- | ------------- | -------- | -------- | ------------------------------------------------------------------ |
| title                 | string        | null     | no       | modal title                                                        |
| data                  | array object  | []       | yes      | modal list object                                                  |
| onPress               | function      | null     | no       | just onpress                                                       |
| withAvatar            | bool          | false    | no       | if you show the items with the avatar or img, you need to set true |
| disabledshowItemCount | int           | 3        | No       | show item count in modal List                                      |
| titleColName          | string        | 'title'  | yes      | key name of the list title item                                    |
| subtitleColName       | str or int    | subtitle | no       | key name of the list subtitle item                                 |
| imgColName            | source object | null     | no       | list item avatar key name                                          |
| dropdownTitle         | string        | false    | yes      | title of the selected item                                         |
| dropDownSubTitle      | string        | null     | no       | subtitle of the selected item                                      |
| dropDownImg           | source object | null     | no       | avatar of the selected item                                        |
| rightTitle            | str           | null     | no       | list item right title                                              |
| rightSubtitle         | str           | null     | no       | list item right subtitle                                           |
