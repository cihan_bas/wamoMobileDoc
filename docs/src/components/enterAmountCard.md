Enter Amount Card is used for money request, payment link with virtual keyboard to display amount. There is also 
Text input for add a description.

<p align="center">
  <img src="../../../assets/gif/enterAmountCard.gif" width="200">
</p>

## Import the Enter Amount Card 

```javascript
 
 import {EnterAmountCard} from 'components'
 
```

## Usage of Enter Amount Card and Virtual Keyboard with storybook
```javascript
import React, { useState } from 'react';
import { storiesOf } from '@storybook/react-native';
import { action } from '@storybook/addon-actions';
import EnterAmountCard from './index';
import { colors } from 'config';
import { View } from 'react-native';
import { Keyboard } from 'components/VirtualKeyboard';

storiesOf('Enter Amount Card', module)
  .add('default', (state, setState) => {
    return (
      <EnterAmountCard
        onChangeText={val => this.setState({ amount: val })}
        amount={'13'}
        currency={'eur'}
        placeholder="Add Note"
      />
    );
  })

  .add('withKeyboard', (state, setState) => {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: '#fff',
          justifyContent: 'space-between',
        }}>
        <EnterAmountCard
          onChangeText={val => this.setState({ amount: val })}
          amount={'13'}
          currency={'eur'}
          placeholder="Add Note"
        />

        <View style={{ flex: 3 }}>
          <Keyboard
            onChangeText={val => setState({ amount: val })}
            onPress={console.log('hello')}
            disableBtn={parseFloat('234234234') <= 0}
            btnText={'Continue'}
            onchange={text => console.log(text)}
            loading={false}
            containerStyle={{ display: 'flex' }}
          />
        </View>
      </View>
    );
  });
```
## Props
| name                | type   | default    | is required | description                   |
|---------------------|--------|------------|-------------|-------------------------------|
| onChangeText        | string | null       | no          | Change when press on keyboard |
| amount              | string | null       | no          | amount of the card            |
| subtitle            | string | null       | no          | Add subtitle if required      |
| onChangeDescription | string | null       | no          | Description of the card       |
| currency            | object | ''         | no          | currency icon for amount      |
| placeholder         | string | 'Add Note' | no          | Description placeholder       | 