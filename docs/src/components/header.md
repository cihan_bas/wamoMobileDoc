Header component displays header of app.

<p align="center">
  <img src="../../../assets/gif/header.gif" width="200">
</p>

## Import the header 

```javascript
 
 import {Header} from 'components'
 
```

## Usage with storybook

```javascript
 
 import React from 'react';
 import { storiesOf } from '@storybook/react-native';
 import { action } from '@storybook/addon-actions';
 import Header from './index';
 import { BufferView } from '../decorator';
 
 storiesOf('Header', module)
   .add('noneValue', () => {
     return <Header />;
   })
   .add('primary', () => {
     return <Header text="Check Password" isPrimary />;
   })
   .add('secondary', () => {
     return <Header text="Check Password" containerType="secondaryContainer" />;
   })
   .add('default', () => {
     return <Header text="Check Password" />;
   })
   .add('rightIcon', () => {
     return (
       <Header
         text="Check Password"
         rightIconName="search"
         onRight={action('tapped-ONRIGHT icon')}
       />
     );
   })
   .add('rightText', () => {
     return (
       <Header
         text="Check Password"
         rightText="close"
         onRight={action('tapped-ONRIGHT icon')}
       />
     );
   });

```

## Props 
| name          | type                                 | default      | is required | description                   |
|---------------|--------------------------------------|--------------|-------------|-------------------------------|
| isPrimary     | bool                                 | false        | no          | header container type         |
| hideLeft      | bool                                 | false        | no          | hide left arrow if it is true |
| text          | string                               | ''           | no          | text of header                |
| containerType | primaryContainer, secondaryContainer | ''           | no          | type of container             |
| rightIconName | string                               | ''           | no          | right icon name               |
| rightIconType | string                               | font-awesome | no          | font type of right icon       |
| rightText     | string                               | ''           | no          | right text                    |
| leftIconName  | string                               | chevron-left | no          | left icon name                |
| leftIconType  | string                               | feather      | no          | font type of left icon        |
| leftText      | string                               | ''           | no          | left text                     |