Header Card component displays header menu of app.

<p align="center">
  <img src="../../../assets/gif/headercard.gif" width="200">
</p>

## Import the headercard

```javascript
 
 import {HeaderCard} from 'components'
 
```

## Usage with storybook
```javascript
 
 import React from 'react';
 import { storiesOf } from '@storybook/react-native';
 import HeaderCard from './index';
 import { action } from '@storybook/addon-actions';
 import { BufferView } from '../decorator';
 import { withKnobs } from '@storybook/addon-knobs';
 const Bank = require('assets/img/glyph/Bank.png');
 const Payment = require('assets/img/glyph/Payment.png');
 const Recurring = require('assets/img/glyph/Recurring.png');
 const Tag = require('assets/img/glyph/Tag.png');
 const topList = [
   {
     title: 'Bank transfer',
     icon: 'bank',
     iconType: 'material-community',
     key: 'BankTransfer',
     image: Bank,
   },
   {
     title: 'Tap to Tag',
     icon: 'wifi',
     iconType: 'feather',
     key: null,
     image: Tag,
   },
 ];
 
 storiesOf('HeaderCard', module)
   .addDecorator(BufferView)
   .add('defaultHeader', () => {
     return <HeaderCard topList={topList} />;
   });
 export default {
   title: 'HeaderCard',
   parameters: {
     notes: 'some documentation here',
   },
 };

```

## Props 
| name    | type  | default | is required | description              |
|---------|-------|---------|-------------|--------------------------|
| topList | array | []      | no          | list of the header menu  |