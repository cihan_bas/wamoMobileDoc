Inputs allow users to enter text into a UI. They typically appear in forms and dialogs.

## screenshot

<img src="../../../assets/gif/input.gif" alt="input" width="200"/>

## import input

```javascript
import { Input } from 'components';
```

## Example

```javascript
<Input
	label={this.state.nameLabel}
	placeholder={this.nameInputPlaceHolder}
	inputPadding={10}
	hasError={this.state.haserror}
	errorMessage={this.state.nameInputErrorMsg}
/>
```

## Example for edit

```javascript
<Input
	label={this.state.nameLabel}
	placeholder={this.nameInputPlaceHolder}
	inputPadding={10}
	hasError={this.state.haserror}
	errorMessage={this.state.nameInputErrorMsg}
	showEdit
	value="Hi i'm not editable"
	editable={false}
	onEdit={() => alert('onEdit')}
/>
```

## Props

| name         | type     | default | required | description                                                                                                                       |
| ------------ | -------- | ------- | -------- | --------------------------------------------------------------------------------------------------------------------------------- |
| label        | string   | null    | yes      | input label text                                                                                                                  |
| value        | string   | ' '     | yes      | input value                                                                                                                       |
| onchange     | func     | null    | yes      | Callback that is called when the text input's text changes. This will be called with { nativeEvent: { eventCount, target, text} } |
| hasError     | bool     | false   | no       | If true change label and border color to `red`                                                                                    |
| errorMessage | string   | ' '     | no       | Error message                                                                                                                     |
| placeholder  | string   | ' '     | no       | input placeholder                                                                                                                 |
| showEdit     | bool     | false   | no       | show edit button                                                                                                                  |
| onEdit       | function | void 0  | no       | onPress edit button callback function                                                                                             |
| editable     | bool     | true    | no       | If false, text is not editable. The default value is true.                                                                        |
