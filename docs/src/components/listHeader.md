Text displays words and characters at various sizes.

<div style='text-align:center'>
  <img src="../../../assets/img/listHeader.png" width="200" alt = 'list Header' >
</div>

## Import the List Header

```javascript
import { ListHeader } from 'components';
```

```javascript
<ListHeader
	leftTitle='Today'
	type='DEPOSIT'
	currency='eur'
	hasIcon
	amount='250'
/>
```

## Props

| name       | type       | default  | required | description                                |
| ---------- | ---------- | -------- | -------- | ------------------------------------------ |
| rightTitle | string     | null     | no       | right text                                 |
| leftTitle  | string     | ' '      | yes      | left text                                  |
| type       | string     | WITHDRAW | no       | DEPOSIT or WITHDRAW                        |
| currency   | string     | ' '      | no       | currency text `{"eur","btc","eth","gbp" }` |
| amount     | str or int | ' '      | no       | amount                                     |
| hasIcon    | bool       | false    | no       | if true show currency                      |
