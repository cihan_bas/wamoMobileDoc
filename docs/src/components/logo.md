## screenshot

<div style='text-align:center'>
  <img src="../../../assets/gif/logo.gif" width="200" alt = 'wamo logo' >
</div>
## import Logo

```javascript
import { Logo } from 'components';
```

## Example

```javascript
<Logo isPrimary={true} />
```

## Props

| name      | type | default | required | description               |
| --------- | ---- | ------- | -------- | ------------------------- |
| isPrimary | bool | false   | no       | if true show primary logo |
