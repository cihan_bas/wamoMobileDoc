Picker component makes list pickable and searchable .

<p align="center">
  <img src="../../../assets/gif/pickeritem.gif" width="200">
</p>

## Import the pickeritem 

```javascript
 
 import {PickerItem} from 'components'
 
```

## Usage with storybook

```javascript
 
 import React from 'react';
 import { storiesOf } from '@storybook/react-native';
 import Picker from './index';
 import { action } from '@storybook/addon-actions';
 import { withKnobs } from '@storybook/addon-knobs';
 const btc = require('assets/img/icons/currencies/btc.png');
 const eth = require('assets/img/icons/currencies/eth.png');
 const eur = require('assets/img/icons/currencies/eur.png');
 const list = [
   {
     title: 'BTC',
     img: btc,
     subtitle: 2205,
     id: '1',
   },
   {
     title: 'ETH',
     img: eth,
     subtitle: 1500,
     id: '2',
   },
 ];
 storiesOf('Picker', module)
   .addDecorator(withKnobs)
   .add('defaultPicker', () => {
     return <Picker data={list} withAvatar onPress={() => action('clicked')} />;
   })
 
   .add('none', () => {
     return <Picker />;
   });
 export default {
   title: 'Picker',
   parameters: {
     notes: 'some documentation here',
   },
 };

```

## Props 
| name             | type           | default  | is required | description                            |
|------------------|----------------|----------|-------------|----------------------------------------|
| showItemCount    | number         | 3        | no          |                                        |
| titleColName     | string         | title    | no          | title of the picker item               |
| subtitleColName  | string         | subtitle | no          | subtitle of the picker item            |
| data             | array          | null     | no          | data for picker                        |
| imgColName       | string         | null     | no          | image source link                      |
| title            | string         | ''       | no          | list item title                        |
| onPress          | function       | null     | yes         | action when on press the picker item   |
| withAvatar       | bool           | false    | no          | if true avatar will be on left         |
| rightTitle       | string         | null     | no          | title of picker item on right side     |
| rightSubtitle    | string         | null     | no          | subtitle of picker item on right side  |
| dropdownTitle    | string         | ''       | no          | title of picker item when drop down    |
| dropDownSubTitle | string         | null     | no          | subtitle of picker item when drop down |
| dropDownImg      | number, string | null     | no          | image of picker item when drop down    |