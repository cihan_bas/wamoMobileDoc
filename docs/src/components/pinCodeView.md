A customizable PIN Code component for react native

Using:

1. react-native-keychain to store the pin in Keychain/Keystore
2. react-native-touch-id to authenticate users with FaceID/TouchID
3. react-native-vector-icons to use the material icons

#Usage
Basic usage requires choosing between the choose, enter and locked modes.

* choose : requires the user to choose and to confirm a PIN code
* enter : requires the user to enter the PIN code he previously chose
* locked : prints a locked screen for a given time if the user failed to enter his/her PIN code too many times

```json
PinCode.propTypes = {
  errorText: PropTypes.string,
  passcode_did_not_match: PropTypes.string,
  forgetText: PropTypes.string,
  onPressForget: PropTypes.func,
  onComplete: PropTypes.func.isRequired,
  onSuccess: PropTypes.func,
  headerProps: PropTypes.object,
  status: PropTypes.oneOf([
    pinStatus.enter,
    pinStatus.choose,
    pinStatus.locked,
  ]),
  responseType: PropTypes.oneOf([
    responseType.initial,
    responseType.success,
    responseType.fail,
    responseType.locked,
  ]),
  headerText: PropTypes.string,
  confirmChooseHeader: PropTypes.string,
  subtitle: PropTypes.string,
  chooseConfirmSubtitle: PropTypes.string,
  lockTitle: PropTypes.string,
  lockSubtitle: PropTypes.string,
  TouchableComponent: PropTypes.elementType,
}
```

##Example Login 
```javascript
import { PinCode } from 'components'
<PinCode
    ref={this.ref_pin_code}
    status={this.state.status}
    forgetText={lang.forgot}
    onPressForget={this.forget}
    onComplete={code => this.onComplete(code)}
    responseType={this.state.responseType}
    headerText={lang.enter_your_passcode}
    subtitle=""
    errorText={this.props.error?.msg}
    isDisable={this.props.isDisable}
/>
```

##Example Choose 
```javascript
import { PinCode } from 'components'
<PinCode
      ref={this.ref_pin_code}
      status={'choose'}
      onComplete={code => this.onComplete(code)}
      headerText={lang.create_passcode_header}
      confirmChooseHeader={lang.confirm_passcode}
      subtitle=""
      errorText={this.props.error?.msg}
      passcode_did_not_match={lang.passcode_did_not_match}
/>
```

#Lock Screen

for lock screen we control the fail attempt and isDisable feature
if disable is true we dont control the attempt, we are disabling the app


```javascript

    if (isDisable) {
      this.setState({
        status: 'locked',
        isDisable: true,
      });
    } else {
      if (attempt === 3) {
        this.writeLockStore(attempt);
        this.set_lock(delayforThreeAttempts);
      } else if (attempt === 6) {
        this.writeLockStore(attempt);
        this.set_lock(delayforSixAttempts);
      } else if (attempt > 6) {
        this.writeLockStore(attempt);
        this.set_lock(delayforThreeAttempts);
      }
    }
```

#Props
```javascript
locked.defaultProps = {
  lockTitle: 'Wamo is locked',
  lockSubtitle: 'please try after lock time',
  isDisable: false,
  lockTime: 0,
  disableButtonText: 'Contact Wamo Support',
  disableButtonPress: () => console.log('pressed'),
};  
```