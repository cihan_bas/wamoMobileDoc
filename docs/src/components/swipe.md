This component allows for implementing swipeable rows or similar interaction. It renders its children within a panable container allows for horizontal swiping left and right. While swiping one of two "action" containers can be shown depends on whether user swipes left or right (containers can be rendered by renderLeftActions or renderRightActions props).

<p align="center">
  <img src="../../../assets/gif/swipe.gif" width="200" align="center">
</p>


## Import the swipe 

```javascript
 
 import {Swipe} from 'components'
 
```

## Usage with storybook

```javascript
 
 import React from 'react';
 import { storiesOf } from '@storybook/react-native';
 import { action } from '@storybook/addon-actions';
 import SwipeRow from './index';
 import BankAccount from '../BankAccount';
 import { BufferView } from '../decorator';
 import {
   ScrollView,
   SafeAreaView,
   FlatList,
   View,
   StyleSheet,
 } from 'react-native';
 import { colors } from 'config';
 const singleObject = [
   {
     text: 'Delete',
     color: 'red',
     onPress: text => alert('delete clicked', text),
   },
 ];
 const multipleObject = [
   {
     text: 'View',
     color: colors.primary,
     onPress: text => alert(text),
   },
   {
     text: 'Hide',
     color: colors.yellow,
     onPress: text => alert('Hide clicked', text),
   },
   {
     text: 'Delete',
     color: colors.error,
     onPress: text => alert('delete clicked', text),
   },
 ];
 storiesOf('Swipeable', module)
   .add('Single', () => {
     return (
       <SafeAreaView>
         <SwipeRow rightElements={singleObject} index={0}>
           <BankAccount
             title="Spotify Premium"
             subtitle="Subscriptions"
             num={2458}
           />
         </SwipeRow>
       </SafeAreaView>
     );
   })
   .add('Multiple', () => {
     return (
       <SafeAreaView>
         <FlatList
           data={DATA}
           ItemSeparatorComponent={() => <View style={styles.separator} />}
           renderItem={({ item, index }) => (
             <SwipeRow
               rightElements={multipleObject}
               index={index}
               id={item.from}>
               <BankAccount
                 title={item.from}
                 subtitle={item.message}
                 num={item.when}
                 onPress={() => alert(item.from)}
               />
             </SwipeRow>
           )}
           keyExtractor={(item, index) => `message ${index}`}
         />
       </SafeAreaView>
     );
   });
 const styles = StyleSheet.create({
   rectButton: {
     flex: 1,
     height: 80,
     paddingHorizontal: 20,
     paddingVertical: 20,
     justifyContent: 'space-between',
     flexDirection: 'column',
     backgroundColor: 'white',
   },
   separator: {
     backgroundColor: 'rgb(200, 199, 204)',
     height: StyleSheet.hairlineWidth,
   },
   fromText: {
     fontWeight: 'bold',
     backgroundColor: 'transparent',
   },
   messageText: {
     color: '#999',
     backgroundColor: 'transparent',
   },
   dateText: {
     backgroundColor: 'transparent',
     position: 'absolute',
     right: 20,
     top: 10,
     color: '#999',
     fontWeight: 'bold',
   },
 });
 
 const DATA = [
   {
     from: "D'Artagnan",
     when: '3:11 PM',
     message:
       'Unus pro omnibus, omnes pro uno. Nunc scelerisque, massa non lacinia porta, quam odio dapibus enim, nec tincidunt dolor leo non neque',
   },
   {
     from: 'Aramis',
     when: '11:46 AM',
     message:
       'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vivamus hendrerit ligula dignissim maximus aliquet. Integer tincidunt, tortor at finibus molestie, ex tellus laoreet libero, lobortis consectetur nisl diam viverra justo.',
   },
   {
     from: 'Athos',
     when: '6:06 AM',
     message:
       'Sed non arcu ullamcorper, eleifend velit eu, tristique metus. Duis id sapien eu orci varius malesuada et ac ipsum. Ut a magna vel urna tristique sagittis et dapibus augue. Vivamus non mauris a turpis auctor sagittis vitae vel ex. Curabitur accumsan quis mauris quis venenatis.',
   },
 ];

```

## Props 
| name          | type           | default | is required | description                           |
|---------------|----------------|---------|-------------|---------------------------------------|
| rightElements | array          | null    | yes         | shows the buttons when you swipe left |
| children      | element        | null    | yes         | what is the context of swipe          |
| index         | number         | 0       | yes         | shows index number                    |
| id            | string, number | null    | no          | id                                    |