Text displays words and characters at various sizes.

<p align="center">
  <img src="../../../assets/gif/text.gif" width="200" align="center">
</p>


## Import the text 

```javascript
 
 import {Text} from 'components'
 
```

## Usage with storybook


```javascript
 import React from 'react';
 import { storiesOf } from '@storybook/react-native';
 import { action } from '@storybook/addon-actions';
 import Text, { Typography } from './index';
 import { BufferView } from '../decorator';
 import { ScrollView } from 'react-native';
 const randomString = 'qhjlmtğüşçöf';
 storiesOf('Text', module)
   .addDecorator(BufferView)
   .add('Typography', () => {
     return (
       <ScrollView>
         {Object.values(Typography).map(item => (
           <Text numberOfLines={1} text={item + ' ' + randomString} type={item} key={item} />
         ))}
       </ScrollView>
     );
   })
   .add('defaultText', () => {
     return <Text text="Default" />;
   })
   .add('Customize', () => {
     return (
       <Text
         text="Default"
         textAlign="left"
         style={{
           padding: 25
         }}
         color="green"
       />
     );
   });
```

## Props 
| name     	| type   	| default 	| is required 	| description                                                                  	|
|----------	|--------	|---------	|-------------	|------------------------------------------------------------------------------	|
| text   	| string   	| null   	| yes          	| text                        |
| color    	| string 	| ''      	| no         	| text color                                                                  	|
| style    	| object 	| null    	| no          	| text style     	|
| loading  	| bool   	| false   	| no          	| text loading                                                               	|
| numberOfLines | number  | null   	| no          	| how many lines text should be  	|
| type  	| func   	| 'PL'    	| no         	| text type                                                                 	|
