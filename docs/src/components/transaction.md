Transaction component displays row for each transaction with description.

<p align="center">
  <img src="../../../assets/gif/transaction.gif" width="200">
</p>

## Import the transaction 

```javascript
 import {Transaction} from 'components'
```

## Usage with storybook
```javascript
 import React from 'react';
 import { storiesOf } from '@storybook/react-native';
 import Transaction from './index';
 import { BufferView } from '../decorator';
 const icon = require('assets/img/icons/transactionListAvatar.png');
 const bigIcon = {
   uri:
     'https://cdn.decoist.com/wp-content/uploads/2012/05/showing-collections-in-style.jpg',
 };
 
 storiesOf('Transactions', module)
   .addDecorator(BufferView)
 
   .add('noneValue', () => {
     return <Transaction />;
   })
   .add('fullofData', () => {
     return (
       <Transaction
         source={icon}
         title="Spotify Premium"
         subtitle="Subscriptions"
         type="DEPOSIT"
         amount={2458}
       />
     );
   })
   .add('withAvatar', () => {
     return <Transaction source={bigIcon} />;
   });

```

## Props 
| name          | type                  | default   | is required | description                      |
|---------------|-----------------------|-----------|-------------|----------------------------------|
| source        | number, string        | {}        | no          | source of transaction image      |
| title         | string                | ''        | no          | title of the transaction item    |
| subtitle      | string                | ''        | no          | subtitle of the transaction item |
| bottomDivider | bool                  | true      | no          | divide each transaction          |
| type          | 'DEPOSIT', 'WITHDRAW' | 'DEPOSIT' | no          | transaction type                 |
| currency      | string                | 'eur'     | no          | transaction currency             |
| amount        | number                | 0         | no          | amount of transaction            |