Virtual keyboard used for in app to enter amount. 

<p align="center">
  <img src="../../../assets/gif/enterAmountCard.gif" width="200">
</p>

## Import the Virtual Keyboard

```javascript
 
 import {Keyboard} from 'components'
 
```

## Example

```javascript
 
 <Keyboard onPress={this.props.onPress}
     disableBtn={parseFloat(this.props.amount) > 0 ? false : true}
     btnText={this.props.btnTitle}
     onchange={this.props.onChangeText}
     loading={this.props.loading}
     containerStyle={{ display: this.state.active ? 'flex' : 'none' }}
    />
```

## Props
| name           | type     | default | is required | description                       |
|----------------|----------|---------|-------------|-----------------------------------|
| onPress        | function | null    | no          | Action on button                  |
| btnText        | string   | null    | no          | Text on button                    |
| onchange       | string   | null    | no          | change when click on keyboard     |
| disableBtn     | bool     | null    | no          | disable button to avoid to null   |
| loading        | bool     | null    | no          | show loading when press on button |
| containerStyle | object   | null    | no          | style of virtual keyboard         |