Wallet component displays wallets in user account.

<p align="center">
  <img src="../../../assets/gif/wallet.gif" width="200">
</p>

## Import the wallet 

```javascript
 import {Wallet} from 'components'
```

## Usage with storybook
```javascript
import React from 'react';
import { storiesOf } from '@storybook/react-native';
import Wallets from './index';
import { BufferView } from '../decorator';
import { ScrollView, View } from 'react-native';
const icon = require('assets/img/logo/light/app.png');
import { colors } from 'config';

storiesOf('Wallet', module)
  .addDecorator(BufferView)

  .add('default', () => {
    return <Wallets source={icon} currencyValue={'Btc 20545'} equity="test" />;
  })
  .add('active', () => {
    return (
      <Wallets source={icon} currencyValue={'Btc 20545'} equity="test" active />
    );
  })
  .add('withoutIcon', () => {
    return <Wallets currencyValue={'Btc 20545'} equity="test" />;
  })
  .add('multiple', () => {
    return (
      <ScrollView
        contentContainerStyle={{
          backgroundColor: colors.secondary,
          flexGrow: 1,
        }}
        horizontal>
        <Wallets source={icon} active currencyValue={'Btc 20545'} />
        <Wallets source={icon} equity="234234" />
        <Wallets currencyValue={'Btc 20545'} equity="54353" />
        <Wallets currencyValue={'Btc 20545'} />
        <Wallets currencyValue={'Btc 2054500'} equity="10" />
      </ScrollView>
    );
  });

```

## Props 
| name          | type   | default | is required | description            |
|---------------|--------|---------|-------------|------------------------|
| source        | number | null    | no          | source of wallet image |
| currencyValue | string | ''      | no          | value of the wallet    |
| equity        | string | ''      | no          | type of currency       |
| active        | bool   | false   | no          | if wallet active       |