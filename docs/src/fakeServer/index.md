##Getting Started
Install JSON Server
```textmate 
npm install -g json-server
```
Then 
```textmate  
npm install -D json-server-auth
```
>JWT authentication middleware for JSON Server

##Create db
Create a db.json file with some data
```json
{
  "posts": [
    { "id": 1, "title": "json-server", "author": "typicode" }
  ],
  "comments": [
    { "id": 1, "body": "some comment", "postId": 1 }
  ],
  "profile": { "name": "typicode" },
  "users": []
}
```
 Start JSON Server (Terminal)

```textmate
json-server --watch db.json

```

##More Information 
please look at this [docs ](https://github.com/typicode/json-server) for more information,

for Register and login look at this [docs ](https://github.com/jeremyben/json-server-auth)   
##Example (Postman Collection)
[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/b3154362457653cbbbe1)

 