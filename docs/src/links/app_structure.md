https://medium.com/better-programming/my-awesome-react-redux-structure-6044e5007e22

https://levelup.gitconnected.com/structure-your-react-redux-project-for-scalability-and-maintainability-618ad82e32b7

store
|__ events.ts
|__ sagas.ts
|__ current
| |__ actions.ts
| |__ reducer.ts
| |__ sagas.ts
| |__ selectors.ts
|__ notifications
  |__ actions.ts
  |__ reducer.ts
  |__ selectors.ts
