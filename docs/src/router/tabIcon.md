wamo tab bar on the bottom of the screen that lets you switch between different routes with animation.

<p align="center">
  <img src="../../../assets/gif/tabIcon.gif" width="200">
</p>

## Example

```javascript
<Scene
	key='tabs'
	tabs
	tabBarStyle={styles.tabBar}
	tabBarPosition='bottom'
	hideNavBar
	labelStyle={styles.tabTitle}
	activeLabelStyle={{ color: colors.primary }}
	activeTintColor={colors.primary}>
	<Scene
		key='Home'
		component={Dashboard}
		hideNavBar
		icon={TabIcon}
		activeIcon={homeIcon}
		passiveIcon={homeIconPassive}
	/>
	<Scene
		key='Analaytics'
		component={Wallets}
		hideNavBar
		icon={TabIcon}
		activeIcon={analyticIcon}
		passiveIcon={analyticIconPassive}
	/>
	<Scene
		key='Payments'
		hideNavBar
		component={Wallets}
		activeIcon={paymentsIcon}
		passiveIcon={paymentsIconPassive}
		icon={TabIcon}
	/>
	<Scene
		key='Activity'
		icon={TabIcon}
		component={Dashboard}
		type={ActionConst.RESET}
		hideNavBar
		activeIcon={activityIcon}
		passiveIcon={activityIconPassive}
	/>
	<Scene
		key='More'
		icon={TabIcon}
		component={Dashboard}
		type={ActionConst.RESET}
		hideNavBar
		activeIcon={moreIcon}
		passiveIcon={moreIconPassive}
	/>
</Scene>
```

## icon component

```javascript
export const TabIcon = ({
	focused = false,
	activeIcon,
	passiveIcon,
	showNotification
}) => {
	return (
		<View>
			<Image
				style={styles.tabImage}
				source={focused ? activeIcon : passiveIcon}
			/>
			{showNotification === true ? (
				<Badge status='error' containerStyle={styles.notification} />
			) : null}
		</View>
	);
};
```

## Props

| name             | type         | default | is required | description                                |
| ---------------- | ------------ | ------- | ----------- | ------------------------------------------ |
| icon             | component    | null    | yes         | icon component                             |
| activeIcon       | image source | ''      | yes         | active icon source                         |
| passiveIcon      | image source | ''      | yes         | passive icon source                        |
| focused          | bool         | false   | no          | when click the tabs it set true            |
| showNotification | bool         | false   | no          | show notification ball, it is for activity |
