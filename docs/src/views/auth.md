##Register
After the user verifies his phone number, he/she is directed to the register page to enter the required information,
 where he/she is asked to create a password when he/she completes the required fields. 
 He will use this password in later entries.

##Biometric
When the user logs in for the second time with the password he has created, 
it is checked whether the phone has a biometric support or not.
If there is support and there is a previously created login method, 
the user is asked the question with the alert message whether you would like to open the application with biometric,
 if the user accepts, we want it to be verified,
  if they are correct, we save it in the system and for the next entries, 
  he/she can login with biometric, after 3 incorrect entries, 
  the system will be disabled (this may vary), if it is disabled, 
  it can open and use the application if it enters its password manually.
  
If the application previously activated biometric and tried to open the application on another phone, 
the biometric will not be active, either it will go to the settings/security window to activate  
or when it opens the application for the second time, 
it will confirm and verify the window we will ask if it wants to use biometric.

The reason for this is that when someone else sees the message that comes to your phone, 
they can read with their fingerprints/faceid without using a password and use the application with your account,
 such a way was considered to prevent this.
 
 Note: There are biometric risks
 
 1. On a phone with a defined biometric, the person can add their own fingerprint/faceid and open the application with that fingerprint/faceid, 
 without a password.
 2. On a phone, more than one person's fingerprint/faceid can be active, and these people can activate biometrics and enter the application.

##Login
The user can login to the application with a password or biometric. The user logs off periodically, after logs off, 
he/she can continue using the application by verifying the phone number again and then entering the password.