After started page routed on this page, the user will enter the phone number with country code(Default country code will be user's location country).


<p align="center">
  <img src="../../../assets/gif/checkphonenumber.gif" width="200">
</p>
 
 
## Container 
```reactjs
import { connect } from 'react-redux';
import CheckPhoneNumber from './index';
import { checkPhoneNumber } from '@checkPhoneNumber/actions';
import { getCountries } from '@started/actions';
const mapStateToProps = state => {
  return {
    loader: state.checkPhoneNumber.loader,
    userId: state.checkPhoneNumber.userId,
    checkPhoneNumberError: state.checkPhoneNumber.checkPhoneNumberError,
    country: state.started.country,
    lang: state.translate.lang,
  };
};
const mapStateToDispatch = dispatch => {
  return {
    checkPhoneNumber: data => dispatch(checkPhoneNumber(data)),
    getCountries: () => dispatch(getCountries()),
  };
};

export default connect(mapStateToProps, mapStateToDispatch)(CheckPhoneNumber);
```

## Api Endpoints
- [PhoneNumberCheck](../api/checkPhoneNumberApi.md)
```reactjs
phoneNumberCheck(data) {
  return axios.post('PhoneNumberCheck', data);
}
```

## Props Details

| name                  | description                                  |
|-----------------------|----------------------------------------------|
| loader                | button loader until api respond              |
| userId                | get user id after check phone number success |
| checkPhoneNumberError | define error if api respond error            |
| country               | get country list for country codes           |
| lang                  | multiple language support                    |