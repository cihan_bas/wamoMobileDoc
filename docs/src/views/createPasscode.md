#Create PassCode
##Props 
```json
{
    lang: state.translate.lang,
    data: state.createPasscode.data,
    error: state.createPasscode.error,
    loading: state.createPasscode.loading,
    userId: state.enterSmsCode.userId,
    userToken: state.enterSmsCode.userToken,
  }
```
## Function
```javascript
    const payload = {
      userId,
      userToken,
      passCode,
      //deviceInfo: DeviceInfo
    };
    create_passcode: payload => dispatch(create_passcode(payload)),

```
##