Enter sms code screen come up after check phone number returned success message from api. 

<p align="center">
  <img src="../../../assets/gif/enterSmsCode.gif" width="200">
</p>
 

## Container 
```reactjs
import { connect } from 'react-redux';
import EnterSmsCode from './index';
import { enterSmsCode, resendSms } from '@enterSmsCode/actions';
const mapStateToProps = state => {
  return {
    lang: state.translate.lang,
    failSmsCode: state.enterSmsCode.failSmsCode,
    resend: state.enterSmsCode.resend,
    userToken: state.enterSmsCode.userToken,

    userId: state.checkPhoneNumber.userId,
    smsCodeLoader: state.checkPhoneNumber.smsCodeLoader,
    isResendCode: state.checkPhoneNumber.isResendCode,
    userRegistered: state.checkPhoneNumber.userRegistered,
    emptyPasscode: state.checkPhoneNumber.emptyPasscode,
    mobileNumber: state.checkPhoneNumber.mobileNumber,
    mobileCountry: state.checkPhoneNumber.mobileCountry,
  };
};
const mapStateToDispatch = dispatch => {
  return {
    enterSmsCode: data => dispatch(enterSmsCode(data)),
    resendSms: data => dispatch(resendSms(data)),
  };
};

export default connect(mapStateToProps, mapStateToDispatch)(EnterSmsCode);
```

## Api Endpoints
- [AuthUserSmsCode](../api/authSmsCodeApi.md)
- [PhoneNumberCheckResendSMS](../api/authSmsCodeApi.md)
```reactjs
authUserSmsCode(data) {
  return axios.post('AuthUserSmsCode', data);
},
phoneNumberCheckResendSMS(data) {
  return axios.post('PhoneNumberCheckResendSMS', data);
},
```

## Props Details

| name           | description                    |
|----------------|--------------------------------|
| lang           | multiple language support      |
| failSmsCode    | If user enter wrong sms code   |
| resend         | If user not receive sms code   |
| userToken      | If user success user has token |
| userId         | User's id                      |
| userRegistered | Check if user is registered    |
| emptyPasscode  | Check user has pin code before |
| mobileNumber   | User's mobile number           |
| mobileCountry  | User's country code            |
