#Login
<div align="center">
  <img src="../../../assets/gif/viewsLogin.gif" width="200">
</div>

##### Our login screen has some features

1. Biometric login (If user allows the permission it can use this feature)
3. Background timer (When app goes to the background then we start a timer and if the timer ended we locked the screen)
4. Lock and disable (We will lock the app if the user password fails more than 3 times and if it fails with more than 6 incorrect login, the app will lock for 1 minute on every password attempt)
5. Keychain/KeyStore (For save the token) 

For login page we have three functions

* When user enter sms code, he/she has not token, so he/she must login with userid, user token and passcode (via sha1)
```javascript
    let enter_code = await sha1(code);
    enter_code = enter_code.toLowerCase();
    const data = {
        passCode: enter_code,
        userId: this.props.userId,
        userToken: this.props.userToken,
      };
      this.props.login(data);

```
    > this api response get token and we save the token in the keychain

* user login with token 
```javascript
   let enter_code = await sha1(code);
    enter_code = enter_code.toLowerCase();
    if (this.props.token) {
      const data = {
        code: enter_code,
      };
      this.props.passcodeControl(data);
    } 
``` 
* user login via biometric
```javascript
    if (auth === true) {
        const token = this.props.token
        this.props.biometric_login(token);
        Actions.home();
    }
```

##Props
```javascript
import { login, biometric_login, passcodeControl } from '@login/actions';
const mapStateToProps = state => {
  return {
    lang: state.translate.lang,
    success: state.login.data,
    token: state.login.token,
    error: state.login.error,
    isDisable: state.login.isDisable,
    loading: state.login.loading,
    userId: state.enterSmsCode.userId,
    userToken: state.enterSmsCode.userToken,
  };
};
const mapStateToDispatch = dispatch => {
  return {
    login: data => dispatch(login(data)),
    passcodeControl: data => dispatch(passcodeControl(data)),
    biometric_login: data => dispatch(biometric_login(data)),
  };
};

```