After sms code success if user not registered before this page come up to user register.

<p align="center">
  <img src="../../../assets/gif/register.gif" width="200">
</p>
 

## Container 
```reactjs
import { connect } from 'react-redux';
import Registration from './index';
import { registerUser, getDocuments } from '@registration/actions';
const mapStateToProps = state => {
  return {
    lang: state.translate.lang,
    data: state.registerUser.data,
    error: state.registerUser.error,
    success: state.registerUser.success,
    country: state.checkPhoneNumber.country,
    userId: state.enterSmsCode.userId,
    userToken: state.enterSmsCode.userToken,
    documents: state.registerUser.documents,
  };
};
const mapStateToDispatch = dispatch => {
  return {
    registerUser: data => dispatch(registerUser(data)),

    getDocuments: () => dispatch(getDocuments()),
  };
};

export default connect(mapStateToProps, mapStateToDispatch)(Registration);

```

## Api Endpoints
- [GetDocuments](../api/registerUserApi.md)
- [RegisterUser](../api/registerUserApi.md)
```reactjs
getDocuments() {
  return axios.get(`GetDocuments`);
},
registerUser(data) {
  return axios.post(`RegisterUser`, data);
},
```

## Props Details
| name      | description                                           |
|-----------|-------------------------------------------------------|
| lang      | multiple language support                             |
| data      | If user register successfully api response            |
| error     | If user register error api response                   |
| success   | If user success register                              |
| country   | User's country                                        |
| userId    | User's id                                             |
| userToken | User's token                                          |
| documents | Documents for privacy policy and terms and conditions |